FROM openjdk:8-jdk-alpine
ADD inventory-service-0.0.1-SNAPSHOT.jar inventory-service-0.0.1-SNAPSHOT.jar
EXPOSE 8282
ENTRYPOINT ["java","-jar","inventory-service-0.0.1-SNAPSHOT.jar"]